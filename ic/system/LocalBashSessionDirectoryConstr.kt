@file:Suppress("NOTHING_TO_INLINE")


package ic.system


import ic.storage.fs.Directory


inline fun LocalBashSession (

	workdir : Directory

) : LocalBashSession {

	return LocalBashSession(workdir.absolutePath)

}