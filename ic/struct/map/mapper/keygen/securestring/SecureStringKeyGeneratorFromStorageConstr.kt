@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.struct.map.mapper.keygen.securestring


import ic.storage.writable.WritableStorage
import ic.struct.map.mapper.keygen.securestring.fromstorage.SecureStringKeyGeneratorFromStorage


inline fun SecureStringKeyGenerator (

	storage : WritableStorage<String>,

	nextIdStorageKey : String = "nextId"

) : SecureStringKeyGeneratorFromStorage {

	return object : SecureStringKeyGeneratorFromStorage() {

		override val storage get() = storage

		override val nextIdStorageKey get() = nextIdStorageKey

	}

}