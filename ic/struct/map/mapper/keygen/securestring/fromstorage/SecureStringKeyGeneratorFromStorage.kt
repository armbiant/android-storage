package ic.struct.map.mapper.keygen.securestring.fromstorage


import ic.base.primitives.int64.Int64
import ic.storage.writable.WritableStorage
import ic.struct.map.mapper.keygen.securestring.SecureStringKeyGenerator


abstract class SecureStringKeyGeneratorFromStorage : SecureStringKeyGenerator() {

	protected abstract val storage : WritableStorage<String>

	protected open val nextIdStorageKey : String get() = "nextId"

	override var nextId : Int64
		get() = storage.getAsInt64OrNull(nextIdStorageKey) ?: 1
		set(value) { storage[nextIdStorageKey] = value }
	;

}