package ic.struct.map.mapper.fromstorage


import ic.interfaces.iterable.ext.copyFilter
import ic.interfaces.iterable.ext.toCountableSet
import ic.storage.writable.WritableStorage
import ic.struct.map.mapper.Mapper


abstract class MapperFromStorage <Key: Any, Value: Any> : Mapper<Key, Value>() {

	protected abstract val storage : WritableStorage<Key>

	override val keys
		@Synchronized
		get() = storage.keys.copyFilter { keyGenerator.isKeyGenerated(it) }.toCountableSet()
	;

	@Synchronized
	override fun implementGetValue (key: Key) : Value? = storage[key]

	@Synchronized
	override fun implementSetValue (key: Key, value: Value?) { storage[key] = value }

}