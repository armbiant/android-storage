@file:Suppress("NOTHING_TO_INLINE")


package ic.struct.map.mapper


import ic.struct.map.mapper.keygen.KeyGenerator

import ic.storage.writable.WritableStorage
import ic.struct.map.mapper.fromstorage.MapperFromStorage


inline fun <Key: Any, Value: Any> Mapper (

	storage : WritableStorage<Key>,

	keyGenerator : KeyGenerator<Key, in Value>

) : MapperFromStorage<Key, Value> {

	return object : MapperFromStorage<Key, Value>() {

		override val keyGenerator get() = keyGenerator

		override val storage get() = storage

	}

}