@file:Suppress("SortModifiers")


package ic.storage


import ic.annotations.ToOverride
import ic.base.throwables.NotExistsException
import ic.interfaces.getter.getter1.untyped.ext.getOrThrowNotExists
import ic.struct.list.List
import ic.struct.map.countable.untyped.UntypedCountableMap


interface Storage <Key> : UntypedCountableMap<Key> {


	operator fun <Value> get (
		key: Key, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>
	) : Value

	override fun <Value: Any?> get (key: Key) : Value


	
	fun getAsInt64OrNull (key: Key) : Long? = get(key)

	
	fun getAsInt64 (key: Key) = getAsInt64OrNull(key)!!


	@ToOverride
	
	@Throws(NotExistsException::class)
	fun isFolderOrThrowNotExists (key: Key) : Boolean {
		val value : Any = getOrThrowNotExists(key)
		return value is Storage<*>
	}

	
	fun isFolder (key: Key) : Boolean {
		try {
			return isFolderOrThrowNotExists(key)
		} catch (t: NotExistsException) {
			throw NotExistsException.Runtime(t)
		}
	}

	
	fun isFolderOrFalse (key: Key) : Boolean {
		try {
			return isFolderOrThrowNotExists(key)
		} catch (t: NotExistsException) {
			return false
		}
	}


}