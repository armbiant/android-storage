package ic.storage.writable.cache


import ic.base.collections.toCountableSet
import ic.base.loop.loop
import ic.storage.writable.BaseWritableStorage
import ic.storage.writable.WritableStorage
import ic.struct.list.List
import ic.struct.map.ephemeral.EphemeralValuesMap
import ic.struct.value.ephemeral.Soft
import ic.base.throwables.Break
import ic.parallel.thread.runInBackground


abstract class CacheWritableStorage<Key: Any> : BaseWritableStorage<Key>() {


	protected abstract val sourceStorage : WritableStorage<Key>


	private val keysCache = kotlin.collections.HashSet<Key>()

	override val keys get() = keysCache.toCountableSet()


	private val valuesCache = EphemeralValuesMap<Key, Any>(::Soft)

	private val keysToWrite = kotlin.collections.HashSet<Key>()

	private var writing : Boolean = false


	override fun implementGet (key: Key, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>) : Any? {
		synchronized(this) {
			if (keysCache.contains(key)) {
				if (valuesCache.containsKey(key)) {
					return valuesCache[key]
				} else {
					val value : Any = sourceStorage[key]
					valuesCache[key] = value
					return value
				}
			} else {
				return null
			}
		}
	}


	override fun set (key: Key, value: Any?) {
		synchronized(this) {
			if (value == null) {
				keysCache.remove(key)
			} else {
				keysCache.add(key)
			}
			if (!keysToWrite.contains(key)) {
				keysToWrite.add(key)
				if (!writing) {
					writing = true
					runInBackground {
						loop {
							val nextKeyToWrite : Key
							synchronized(this) {
								if (keysToWrite.isEmpty()) {
									writing = false
									throw Break
								}
								nextKeyToWrite = keysToWrite.first()
								keysToWrite.remove(nextKeyToWrite)
							}
							sourceStorage[nextKeyToWrite] = valuesCache[nextKeyToWrite]
						}
					}
				}
			}
		}
	}


	override fun createFolderOrThrowAlreadyExists (key: Key) : CacheWritableStorage<Key> {
		return CacheWritableStorage(sourceStorage.createFolderOrThrowAlreadyExists(key))
	}


}