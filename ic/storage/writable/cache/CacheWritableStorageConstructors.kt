@file:Suppress("FunctionName", "NOTHING_TO_INLINE")


package ic.storage.writable.cache


import ic.storage.writable.WritableStorage


inline fun <Key: Any> CacheWritableStorage (sourceWritableStorage : WritableStorage<Key>) : CacheWritableStorage<Key> {

	return object : CacheWritableStorage<Key>() {

		override val sourceStorage get() = sourceWritableStorage

	}

}