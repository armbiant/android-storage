package ic.storage.writable


import ic.annotations.ToOverride
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.interfaces.emptiable.Emptiable
import ic.interfaces.iterable.ext.forEach
import ic.interfaces.remover.Remover1
import ic.storage.Storage
import ic.struct.map.editable.untyped.UntypedEditableMap


interface WritableStorage<Key>
	: Storage<Key>, UntypedEditableMap<Key>, Remover1<Key>, Emptiable
{


	@ToOverride
	override fun empty() = keys.forEach { remove(it) }


	@ToOverride
	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	@Throws(NotExistsException::class)
	override fun removeOrThrowNotExists (key: Key) {
		if (containsKey(key)) {
			set(key, null)
		} else {
			throw NotExistsException
		}
	}


	@Throws(AlreadyExistsException::class)
	fun createFolderOrThrowAlreadyExists (key: Key) : WritableStorage<Key>

	
	fun createFolderIfNotExists (key: Key) : WritableStorage<Key> {
		return try {
			createFolderOrThrowAlreadyExists(key)
		} catch (t: AlreadyExistsException) {
			this[key]
		}
	}

	
	fun createFolder (key: Key) : WritableStorage<Key> {
		try {
			return createFolderOrThrowAlreadyExists(key)
		} catch (t: AlreadyExistsException) { throw AlreadyExistsException.Runtime(t) }
	}


}
