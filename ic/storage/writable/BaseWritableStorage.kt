package ic.storage.writable


import ic.parallel.mutex.Mutex
import ic.storage.BaseStorage


abstract class BaseWritableStorage<Key> : BaseStorage<Key>(), WritableStorage<Key> {

	override val mutex = Mutex()

}