@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.writable.ext


import ic.storage.writable.WritableStorage
import ic.struct.list.List
import ic.struct.list.ext.forEachIndexed
import ic.struct.list.ext.lastIndex


@Suppress("UNCHECKED_CAST")
fun <Key> WritableStorage<Key>.recursiveSet (path: List<Key>, value: Any?) {

	var storage : WritableStorage<Key> = this

	path.forEachIndexed { index, key ->

		if (index == path.lastIndex) {

			storage[key] = value

		} else {

			storage = storage.createFolderIfNotExists(key)

		}

	}

}