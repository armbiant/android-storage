package ic.storage.writable


import ic.storage.stringkey.StringKeyStorage


interface StringKeyWritableStorage : WritableStorage<String>, StringKeyStorage {

	
	override fun createFolderIfNotExists (key: String) = super.createFolderIfNotExists(key) as StringKeyWritableStorage

	
	override fun createFolder (key: String) = super.createFolder(key) as StringKeyWritableStorage

}