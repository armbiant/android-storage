package ic.storage.writable.convertkeys


import ic.annotations.Narrowing
import ic.parallel.mutex.Mutex
import ic.storage.Storage
import ic.storage.convertkeys.ConvertKeysStorage
import ic.storage.writable.WritableStorage


abstract class ConvertKeysWritableStorage <Key, SourceKey> : ConvertKeysStorage<Key, SourceKey>(), WritableStorage<Key> {


	override val mutex = Mutex()


	@Narrowing abstract override val sourceStorage : WritableStorage<SourceKey>


	override fun set (key: Key, value: Any?) {

		sourceStorage[
			convertKeyBack(key, isFolder = value is Storage<*>)
		] = value

	}


	override fun createFolderOrThrowAlreadyExists (key: Key) : WritableStorage<Key> {
		val childSourceStorage = sourceStorage.createFolderOrThrowAlreadyExists(convertKeyBack(key, isFolder = true))
		return object : ConvertKeysWritableStorage<Key, SourceKey>() {
			override val sourceStorage get() = childSourceStorage
			override fun convertKey (key: SourceKey, isFolder: Boolean) = this@ConvertKeysWritableStorage.convertKey(key, isFolder)
			override fun convertKeyBack (key: Key, isFolder: Boolean) = this@ConvertKeysWritableStorage.convertKeyBack(key, isFolder)
		}
	}


}