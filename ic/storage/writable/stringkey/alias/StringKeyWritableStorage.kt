package ic.storage.writable.stringkey.alias


import ic.storage.writable.WritableStorage


typealias StringKeyWritableStorage = WritableStorage<String>