@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.writable.stringkey.ext


import ic.base.primitives.character.Character
import ic.base.strings.ext.split
import ic.storage.writable.ext.recursiveSet
import ic.storage.writable.stringkey.alias.StringKeyWritableStorage


fun StringKeyWritableStorage.recursiveSet (path: String, value: Any?, separator: Character = '/') {
	recursiveSet(
		path.split(separator),
		value
	)
}