package ic.storage.writable.stringkey


import ic.storage.writable.BaseWritableStorage
import ic.storage.writable.StringKeyWritableStorage


abstract class BaseStringKeyWritableStorage : BaseWritableStorage<String>(), StringKeyWritableStorage {



}