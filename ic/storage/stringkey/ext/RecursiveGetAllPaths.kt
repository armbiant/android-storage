package ic.storage.stringkey.ext


import ic.base.primitives.character.Character

import ic.storage.fs.Directory
import ic.storage.stringkey.StringKeyStorage
import ic.struct.collection.Collection
import ic.struct.list.editable.EditableList


fun StringKeyStorage.recursiveGetAllPaths (

	separator : Character = '/',

	encodeName : (String) -> String = { Directory.encodeFileName(it) },

	toIncludeFolders : Boolean,

) : Collection<String> {

	val allPaths = EditableList<String>()

	recursiveForEachPath(
		separator = separator,
		encodeName = encodeName,
		toIncludeFolders = toIncludeFolders
	) {
		allPaths.add(it)
	}

	return allPaths

}