package ic.storage.stringkey.ext


import ic.base.primitives.character.Character
import ic.interfaces.iterable.ext.copyConvert
import ic.interfaces.iterable.ext.join
import ic.storage.ext.recursiveForEachKey
import ic.storage.fs.Directory
import ic.storage.stringkey.StringKeyStorage


fun StringKeyStorage.recursiveForEachPath (

	separator : Character = '/',

	encodeName : (String) -> String = { Directory.encodeFileName(it) },

	toIncludeFolders : Boolean,

	action : (path: String) -> Unit

) {

	recursiveForEachKey (

		toIncludeFolders = toIncludeFolders

	) { path ->

		action(

			path.copyConvert { encodeName(it) }.join(separator = separator)

		)

	}

}