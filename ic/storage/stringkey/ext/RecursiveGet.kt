@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.stringkey.ext


import ic.base.primitives.character.Character
import ic.base.strings.ext.split
import ic.base.throwables.NotExistsException
import ic.storage.ext.recursiveGet
import ic.storage.stringkey.alias.StringKeyStorage


fun <Value> StringKeyStorage.recursiveGet (path: String, separator: Character = '/') : Value {
	return recursiveGet(
		path.split(separator = separator)
	)
}


@Throws(NotExistsException::class)
fun <Value: Any> StringKeyStorage.recursiveGetOrThrowNotExists (path: String) : Value {
	return recursiveGet(path) ?: throw NotExistsException
}