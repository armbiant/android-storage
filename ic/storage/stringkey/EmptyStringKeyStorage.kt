package ic.storage.stringkey


import ic.struct.list.List
import ic.struct.set.countable.CountableSet


class EmptyStringKeyStorage : StringKeyStorage {


	override val keys get() = CountableSet<String>()


	@Suppress("UNCHECKED_CAST")
	override fun <Value> get (key: String, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>) : Value {
		return null as Value
	}

	@Suppress("UNCHECKED_CAST")
	override fun <Value> get (key: String) : Value {
		return null as Value
	}


}