package ic.storage.stringkey.alias


import ic.storage.Storage


typealias StringKeyStorage = Storage<String>