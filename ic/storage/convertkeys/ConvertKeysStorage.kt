package ic.storage.convertkeys


import ic.base.throwables.NotExistsException
import ic.storage.BaseStorage
import ic.storage.Storage
import ic.struct.list.List
import ic.struct.set.countable.ConvertCountableSet
import ic.base.throwables.Skip


abstract class ConvertKeysStorage<Key, SourceKey> : BaseStorage<Key>() {


	protected abstract val sourceStorage : Storage<SourceKey>

	@Throws(Skip::class)
	protected abstract fun convertKey (key: SourceKey, isFolder: Boolean) : Key

	@Throws(Skip::class)
	protected abstract fun convertKeyBack (key: Key, isFolder: Boolean) : SourceKey


	@Throws(NotExistsException::class)
	private fun convertKeyBack (key: Key) : SourceKey {

		try {
			val nonFolderSourceKey = convertKeyBack(key, isFolder = false)
			if (!sourceStorage.isFolderOrThrowNotExists(nonFolderSourceKey)) return nonFolderSourceKey
		} catch (t: Skip) {
		} catch (t: NotExistsException) {}

		try {
			val folderSourceKey = convertKeyBack(key, isFolder = true)
			if (sourceStorage.isFolderOrThrowNotExists(folderSourceKey)) return folderSourceKey
		} catch (t: Skip) {
		} catch (t: NotExistsException) {}

		throw NotExistsException

	}


	override val keys = object : ConvertCountableSet<Key, SourceKey>() {

		override val sourceCountableSet get() = sourceStorage.keys

		@Throws(Skip::class)
		override fun convertItem (sourceItem: SourceKey) : Key {
			return convertKey(
				sourceItem,
				isFolder = (
					try {
						sourceStorage.isFolderOrThrowNotExists(sourceItem)
					} catch (t: NotExistsException) { throw Skip }
				)
			)
		}

		@Throws(Skip::class)
		override fun convertItemBack (item: Key) : SourceKey {
			try {
				return convertKeyBack(item)
			} catch (t: NotExistsException) {
				throw Skip
			}
		}

	}


	override fun implementGet (
		key: Key, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>
	) : Any? {

		return sourceStorage[
			try { convertKeyBack(key) } catch (t: NotExistsException) { return null },
			constructorArgClasses, constructorArgs
		]

	}


}