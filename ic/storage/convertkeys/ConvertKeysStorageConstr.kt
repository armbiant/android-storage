package ic.storage.convertkeys


import ic.storage.Storage


fun <Key, SourceKey> ConvertKeysStorage (

	getSourceStorage : () -> Storage<SourceKey>,

	convertKey : (key: SourceKey, isFolder: Boolean) -> Key,

	convertKeyBack : (key: Key, isFolder: Boolean) -> SourceKey

) :  ConvertKeysStorage<Key, SourceKey> {

	return object : ConvertKeysStorage<Key, SourceKey>() {

		override val sourceStorage get() = getSourceStorage()

		override fun convertKey (key: SourceKey, isFolder: Boolean) = convertKey(key, isFolder)

		override fun convertKeyBack (key: Key, isFolder: Boolean) = convertKeyBack(key, isFolder)

	}

}