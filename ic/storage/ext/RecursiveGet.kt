@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.ext


import ic.interfaces.iterable.ext.forEach
import ic.storage.Storage
import ic.struct.list.List
import ic.base.throwables.Break.Break


@Suppress("UNCHECKED_CAST")
fun <Key, Value> Storage<Key>.recursiveGet (path: List<Key>) : Value {

	var value : Any? = this

	path.forEach { key ->

		if (value == null) Break()

		val valueAsStorage = value as Storage<Key>

		value = valueAsStorage[key]

	}

	return value as Value

}