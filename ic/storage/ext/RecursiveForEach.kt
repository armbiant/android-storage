package ic.storage.ext


import ic.interfaces.iterable.ext.forEach
import ic.storage.Storage
import ic.struct.list.List
import ic.struct.list.ext.plus


fun <Key> Storage<Key>.recursiveForEachKey (

	toIncludeFolders : Boolean,

	action : (path: List<Key>) -> Unit

) {

	keys.forEach { key ->

		val path = List(key)

		if (isFolder(key)) {

			if (toIncludeFolders) {
				action(path)
			}

			val childFolder : Storage<Key> = this[key]

			childFolder.recursiveForEachKey(

				toIncludeFolders = toIncludeFolders

			) { childPath ->

				action(path + childPath)

			}

		} else {

			action(path)

		}

	}

}