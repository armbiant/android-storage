package ic.storage.cache


import ic.storage.BaseStorage
import ic.storage.Storage
import ic.struct.list.List
import ic.struct.map.ephemeral.EphemeralValuesMap
import ic.struct.set.countable.CountableSet
import ic.struct.value.ephemeral.Soft


abstract class CacheStorage<Key> : BaseStorage<Key>() {


	protected abstract val sourceStorage : Storage<Key>


	private var cachedKeys : CountableSet<Key>? = null

	override val keys get() = synchronized(this) {
		if (cachedKeys == null) {
			cachedKeys = sourceStorage.keys
		}
		cachedKeys!!
	}


	private val cache = EphemeralValuesMap<Key, Any>(::Soft)

	override fun implementGet (key: Key, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>) : Any? = synchronized(this) {
		if (containsKey(key)) {
			val valueFromCache = cache[key]
			if (valueFromCache == null) {
				val value : Any? = sourceStorage[key]
				if (value == null) {
					null
				} else {
					cache[key] = value
					value
				}
			} else {
				valueFromCache
			}
		} else {
			null
		}
	}


}