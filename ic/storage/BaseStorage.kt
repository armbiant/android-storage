package ic.storage


import ic.parallel.mutex.Mutex
import ic.struct.list.List
import ic.struct.map.countable.untyped.BaseUntypedCountableMap


abstract class BaseStorage<Key> : BaseUntypedCountableMap<Key>(), Storage<Key> {

	abstract fun implementGet (
		key: Key, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>
	) : Any?

	@Suppress("UNCHECKED_CAST")
	override fun <Value> get(
		key : Key,
		constructorArgClasses : List<Class<*>>,
		constructorArgs : List<Any?>
	) : Value {
		return implementGet(key, constructorArgClasses, constructorArgs) as Value
	}

	override fun implementGet (key: Key) : Any? = get(key, List(), List())

}