package ic.storage.assets.res


import ic.base.reflect.getObjectByNameOrNull
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized
import ic.storage.stringkey.EmptyStringKeyStorage
import ic.storage.stringkey.StringKeyStorage


private val lock = Mutex()

private var cached : StringKeyStorage? = null

val resourcesDirectory : StringKeyStorage get() = lock.synchronized {
	if (cached == null) {
		cached = getObjectByNameOrNull("ic.storage.assets.res.ResourcesDirectoryImplementation") ?: EmptyStringKeyStorage()
	}
	cached!!
}