@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.assets.res.string


fun getResText (path: String) = stringResourceProvider.getResText(path)