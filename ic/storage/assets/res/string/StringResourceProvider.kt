package ic.storage.assets.res.string


import ic.text.Text


interface StringResourceProvider {


	fun getResString (path: String) : String

	fun getResText (path: String) : Text


}