@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.assets.res.string


fun getResString (path: String) = stringResourceProvider.getResString(path)