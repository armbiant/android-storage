package ic.storage.fs


import ic.stream.input.ByteInput
import ic.stream.sequence.ByteSequence
import ic.base.throwables.NotExistsException
import ic.base.throwables.WrongValueException
import ic.stream.input.fromis.ByteInputFromInputStream
import java.io.BufferedInputStream
import java.io.FileInputStream


class File : FileSystemEntry, ByteSequence {

	@Throws(NotExistsException::class, WrongValueException::class)
	constructor (javaFile: java.io.File) : super (javaFile)

	@Throws(NotExistsException::class, WrongValueException::class)
	constructor (path: String) : super (path)

	init {
		if (javaFile.isDirectory) throw WrongValueException
	}

	override fun newIterator() : ByteInput {
		return ByteInputFromInputStream(
			BufferedInputStream(
				FileInputStream(javaFile)
			)
		)
	}

}