@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.fs


import ic.base.arrays.ext.toCountableSet
import ic.base.reflect.className
import ic.storage.writable.WritableStorage
import ic.stream.output.ByteOutput
import ic.struct.collection.convert.ConvertCollection
import ic.struct.set.countable.CountableSet
import ic.storage.writable.stringkey.BaseStringKeyWritableStorage
import ic.storage.writable.StringKeyWritableStorage
import ic.stream.sequence.ByteSequence
import ic.struct.list.List
import ic.base.throwables.*
import ic.interfaces.mutable.ext.synchronized
import ic.parallel.mutex.Mutex
import ic.stream.output.fromos.ByteOutputFromOutputStream
import ic.util.text.charset.url.decodeUrl
import ic.util.text.charset.url.encodeUrl
import ic.util.time.Time
import java.io.BufferedOutputStream
import java.io.FileOutputStream


class Directory : FileSystemEntry, StringKeyWritableStorage {


	override val mutex = Mutex()


	// Base constructors:

	@Throws(NotExistsException::class, WrongValueException::class)
	private constructor (javaFile: java.io.File) : super (javaFile)

	@Throws(NotExistsException::class, WrongValueException::class)
	private constructor (path: String) : super (path)

	init {
		if (!javaFile.isDirectory) throw WrongValueException
	}


	override val keys get() = synchronized {
		javaFile.list()!!.toCountableSet { decodeFileName(it) }
	}


	@Synchronized
	private fun implementGet (key: String) : Any? {
		val childJavaFile = java.io.File(javaFile, encodeFileName(key))
		if (childJavaFile.exists()) {
			if (childJavaFile.isDirectory) {
				return Directory.fromJavaFile(childJavaFile)
			} else {
				return try {
					File(childJavaFile)
				} catch (e: NotExistsException) { throw NotExistsException.Runtime(e)
				} catch (e: WrongValueException) { throw WrongValueException.Runtime(e) }
			}
		} else {
			return null
		}
	}

	@Synchronized
	override fun set (key: String, value: Any?) {
		val childJavaFile = java.io.File(javaFile, encodeFileName(key))
		when (value) {
			null -> {
				childJavaFile.deleteRecursively()
			}
			is FileSystemEntry -> {
				childJavaFile.deleteRecursively()
				value.asJavaFile.copyRecursively(childJavaFile, overwrite = true)
			}
			is ByteSequence -> {
				childJavaFile.deleteRecursively()
				val byteOutput = ByteOutputFromOutputStream(
					BufferedOutputStream(
						FileOutputStream(childJavaFile)
					)
				)
				byteOutput.write(value)
				byteOutput.close()
			}
			else -> throw WrongTypeException.Runtime("value: ${ value.className }")
		}
		setFilePermissions(childJavaFile.absolutePath, isRecursive = value is Directory)
	}


	// BaseStringKeyWritableStorage implementation:

	@Throws(AlreadyExistsException::class)
	override fun createFolderOrThrowAlreadyExists (key: String) : Directory {
		if (keys.contains(key)) throw AlreadyExistsException
		val childJavaFile = java.io.File(javaFile, encodeFileName(key))
		childJavaFile.mkdir()
		setFilePermissions(childJavaFile.absolutePath, isRecursive = false)
		return Directory.fromJavaFile(childJavaFile)
	}

	override fun createFolderIfNotExists (key: String) = super.createFolderIfNotExists(key) as Directory

	override fun createFolder (key: String) : Directory {
		try {
			return createFolderOrThrowAlreadyExists(key)
		} catch (t: AlreadyExistsException) {
			throw AlreadyExistsException.Runtime(
				"Folder $absolutePath/${ encodeFileName(key) } already exists"
			)
		}
	}

	private val baseStringKeyWritableStorage = object : BaseStringKeyWritableStorage() {

		override val keys get() = this@Directory.keys

		override fun createFolderOrThrowAlreadyExists (key: String) : WritableStorage<String> {
			return this@Directory.createFolderOrThrowAlreadyExists(key)
		}

		override fun implementGet (key: String, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>) : Any? {
			return this@Directory.implementGet(key)
		}

		override fun set (key: String, value: Any?) = this@Directory.set(key, value)

	}

	override fun <Value> get (key: String, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>) : Value {
		return baseStringKeyWritableStorage.get(key, constructorArgClasses, constructorArgs)
	}

	override fun <Value> get (key: String) : Value {
		return baseStringKeyWritableStorage.get(key)
	}


	fun getLastModifiedTimeOrNull (key: String) : Time? {
		val childJavaFile = java.io.File(javaFile, encodeFileName(key))
		if (!childJavaFile.exists()) return null
		return Time(childJavaFile.lastModified())
	}


	fun remove() {
		javaFile.deleteRecursively()
	}


	// Symbolic links:

	fun createSymbolicLink (name: String, linkPath: String) {
		ic.storage.fs.createSymbolicLink(
			baseDirectory = this,
			name = name,
			link = linkPath
		)
	}


	override fun toString() = "Directory { absolutePath: $absolutePath }"


	companion object {

		private val URL_ENCODED_PREFIX = "\$URL_ENCODED_UTF_8."

		fun encodeFileName (key: String) = if (
			key.contains('/') 	||
			key.contains('\\')	||
			key.contains('*')	||
			key.contains(';')	||
			key.contains(':')	||
			key.contains('|')	||
			key.contains('\'')	||
			key.contains('"')	||
			key.contains('`')
		) {
			URL_ENCODED_PREFIX + encodeUrl(key)
		} else {
			key
		}

		fun decodeFileName (fileName: String) = if (fileName.startsWith(URL_ENCODED_PREFIX)) {
			decodeUrl(fileName.substring(URL_ENCODED_PREFIX.length))
		} else {
			fileName
		}

		fun fromJavaFile (javaFile: java.io.File) : Directory {
			return try {
				Directory(javaFile)
			} catch (e: NotExistsException) 	{ throw NotExistsException.Runtime(e)
			} catch (e: WrongValueException) 	{ throw WrongValueException.Runtime(e) }
		}

		@Throws(NotExistsException::class)
		fun getExistingOrThrowNotExists (path: String) : Directory {
			return try {
				Directory(path)
			} catch (e: WrongValueException) { throw WrongValueException.Runtime(e) }
		}

		@Throws(NotExistsException::class)
		fun getExistingOrThrowNotExists (baseDir: Directory, path: String) : Directory {
			return try {
				Directory(createJavaFile(baseDir, path))
			} catch (e: WrongValueException) { throw WrongValueException.Runtime(e) }
		}

		fun getExisting (path: String) : Directory {
			return try {
				getExistingOrThrowNotExists(path)
			} catch (e: NotExistsException) { throw NotExistsException.Runtime(e) }
		}

		@Throws(AlreadyExistsException::class)
		fun createOrThrowAlreadyExists (path: String) : Directory {
			val javaFile = java.io.File(path)
			if (javaFile.exists()) throw AlreadyExistsException
			javaFile.mkdirs()
			setFilePermissions(javaFile.absolutePath, isRecursive = false)
			return try {
				Directory(javaFile)
			} catch (e: NotExistsException) 	{ throw NotExistsException.Runtime(e)
			} catch (e: WrongValueException) 	{ throw WrongValueException.Runtime(e) }
		}

		@Throws(AlreadyExistsException::class)
		fun createOrThrowAlreadyExists (baseDir: Directory, path: String) : Directory {
			val javaFile = createJavaFile(baseDir, path)
			if (javaFile.exists()) throw AlreadyExistsException
			javaFile.mkdirs()
			setFilePermissions(javaFile.absolutePath, isRecursive = false)
			return try {
				Directory(javaFile)
			} catch (e: NotExistsException) 	{ throw NotExistsException.Runtime(e)
			} catch (e: WrongValueException) 	{ throw WrongValueException.Runtime(e) }
		}

		inline fun create (baseDir: Directory, path: String) : Directory {
			try {
				return createOrThrowAlreadyExists(baseDir, path)
			} catch (t: AlreadyExistsException) { throw AlreadyExistsException.Runtime(t) }
		}

		inline fun create (path: String) : Directory {
			try {
				return createOrThrowAlreadyExists(path)
			} catch (t: AlreadyExistsException) { throw AlreadyExistsException.Runtime(t) }
		}

		fun createIfNotExists (path: String) : Directory {
			val javaFile = java.io.File(path)
			if (!javaFile.exists()) {
				javaFile.mkdirs()
			}
			setFilePermissions(javaFile.absolutePath, isRecursive = false)
			return try {
				Directory(javaFile)
			} catch (e: NotExistsException) 	{ throw NotExistsException.Runtime(e)
			} catch (e: WrongValueException) 	{ throw WrongValueException.Runtime(e) }
		}

		fun createIfNotExists (baseDir: Directory, path: String) : Directory {
			val javaFile = createJavaFile(baseDir, path)
			if (!javaFile.exists()) {
				javaFile.mkdirs()
			}
			setFilePermissions(javaFile.absolutePath, isRecursive = false)
			return try {
				Directory(javaFile)
			} catch (e: NotExistsException) 	{ throw NotExistsException.Runtime(e)
			} catch (e: WrongValueException) 	{ throw WrongValueException.Runtime(e) }
		}

	}


}