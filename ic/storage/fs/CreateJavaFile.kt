package ic.storage.fs


internal fun createJavaFile (baseDirectory: Directory, path: String) : java.io.File {

	if (

		path.startsWith('/') ||
		path.startsWith('~')

	) {

		return java.io.File(path)

	} else {

		return java.io.File(baseDirectory.javaFile, path)

	}

}