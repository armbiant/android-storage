package ic.storage.fs


import ic.base.reflect.getClassByName
import ic.base.reflect.methods
import ic.base.reflect.staticMethods
import ic.base.throwables.AccessDeniedException
import ic.struct.list.List
import ic.system.isAndroid


private var userHomeDirectoryValue : Directory? = null

val userHomeDirectory : Directory
	@Synchronized get() {
		if (userHomeDirectoryValue == null) {
			userHomeDirectoryValue = (
				if (isAndroid) {
					val thisApp : Any = (
						getClassByName<Any>("ic.android.BaseApplication").staticMethods["getInstance", List()]!!()
					)
					val externalFilesDir : java.io.File = (
						thisApp.methods["getExternalFilesDir", List(String::class.java)]!! (List<Any?>(null))
					)
					Directory.fromJavaFile(externalFilesDir)
				} else {
					Directory.getExisting(System.getProperty("user.home"))
				}
			)
		}
		return userHomeDirectoryValue!!
	}
;


@Throws(AccessDeniedException::class)
fun getUserHomeDirectory (userName: String) = Directory.createIfNotExists(
	if (userName == "root") {
		"/root"
	} else {
		"/home/$userName"
	}
)