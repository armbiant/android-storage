@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.fs


import ic.system.localBashSession


internal inline fun createSymbolicLink (

	baseDirectory : Directory,

	name : String,

	link : String

) {

	val absolutePath = baseDirectory.absolutePath + '/' + Directory.encodeFileName(name)

	localBashSession.executeCommand(
		"ln -s $link $absolutePath"
	)

	setFilePermissions(absolutePath, isRecursive = false)

}