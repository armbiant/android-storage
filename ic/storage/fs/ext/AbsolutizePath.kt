package ic.storage.fs.ext


import ic.storage.fs.Directory


fun Directory.absolutizePath (path: String) : String {
	if (path.startsWith("/")) return path
	if (path.startsWith("~")) return path
	return "$absolutePath/$path"
}