package ic.storage.fs.ext


import java.nio.file.Paths

import ic.storage.fs.Directory


fun Directory.relativizePath (absolutePath: String) : String {
	return Paths.get(this.absolutePath).relativize(
		Paths.get(absolutePath)
	).toString()
}