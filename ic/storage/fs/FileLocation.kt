package ic.storage.fs


import ic.storage.fs.ext.absolutizePath


class FileLocation (

	val baseDirectory : Directory,

	val relativePath : String

) {

	val absolutePath : String get() = baseDirectory.absolutizePath(relativePath)

}