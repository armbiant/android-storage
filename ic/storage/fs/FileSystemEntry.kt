@file:Suppress("ConvertSecondaryConstructorToPrimary")


package ic.storage.fs

import ic.base.throwables.NotExistsException


abstract class FileSystemEntry {


	internal val javaFile : java.io.File

	val asJavaFile get() = javaFile


	val absolutePath : String

	val name : String


	@Throws(NotExistsException::class)
	internal constructor (javaFile: java.io.File) {

		this.javaFile = javaFile

		absolutePath = run {
			var absolutePath : String = javaFile.absolutePath
			if (absolutePath.endsWith("/")) absolutePath = absolutePath.substring(0, absolutePath.length - "/".length)
			absolutePath
		}

		name = javaFile.name

		if (!javaFile.exists()) throw NotExistsException

	}

	@Throws(NotExistsException::class)
	internal constructor (path: String) : this (java.io.File(path))


	val parent : Directory? get() {
		return Directory.fromJavaFile(javaFile.parentFile ?: return null)
	}


}