package ic.storage.fs


import ic.system.isAndroid
import ic.system.isRoot
import ic.system.localBashSession


fun setFilePermissions (absolutePath: String, isRecursive: Boolean) {

	if (isAndroid) return

	if (absolutePath.startsWith("/home/public/")) {

		localBashSession.executeCommand(
			"chmod " +
			(if (isRecursive) "-R " else "") +
			"777 " +
			absolutePath
		)

	} else {

		if (isRoot) {

			if (!absolutePath.startsWith("/home/")) {
				localBashSession.executeCommand(
					"chmod " +
					(if (isRecursive) "-R " else "") +
					"755 " +
					absolutePath
				)
			}

		}

	}

}