package ic.storage.ram


import ic.base.throwables.AlreadyExistsException
import ic.storage.writable.BaseWritableStorage
import ic.struct.list.List
import ic.struct.map.editable.EditableMap


class RamStorage<Key> : BaseWritableStorage<Key>() {

	private val editableMap = EditableMap<Key, Any>()

	override val keys get() = editableMap.keys

	override fun implementGet (key: Key, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>) = editableMap[key]

	override fun set (key: Key, value: Any?) {
		editableMap[key] = value
	}

	@Throws(AlreadyExistsException::class)
	override fun createFolderOrThrowAlreadyExists (key: Key) : RamStorage<Key> {
		val folder = RamStorage<Key>()
		this[key] = folder
		return folder
	}

}