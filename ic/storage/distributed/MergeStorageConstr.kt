@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.distributed


import ic.storage.Storage
import ic.struct.sequence.Sequence


inline fun <Key> MergeStorage (

	crossinline getChildren : () -> Sequence<Storage<Key>>

) : MergeStorage<Key> {

	return object : MergeStorage<Key>() {

		override val children get() = getChildren()

	}

}


inline fun <Key> MergeStorage (

	children : Sequence<Storage<Key>>

) = MergeStorage(

	getChildren = { children }

)