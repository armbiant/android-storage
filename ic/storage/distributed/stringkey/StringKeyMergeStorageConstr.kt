@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.distributed.stringkey


import ic.storage.Storage
import ic.struct.sequence.Sequence


inline fun StringKeyMergeStorage (

	crossinline getChildren : () -> Sequence<Storage<String>>

) : StringKeyMergeStorage {

	return object : StringKeyMergeStorage() {

		override val children get() = getChildren()

	}

}


inline fun StringKeyMergeStorage (

	children : Sequence<Storage<String>>

) = StringKeyMergeStorage(

	getChildren = { children }

)