package ic.storage.distributed.stringkey


import ic.storage.Storage
import ic.storage.distributed.MergeStorage
import ic.storage.stringkey.StringKeyStorage
import ic.struct.sequence.Sequence


abstract class StringKeyMergeStorage : MergeStorage<String>(), StringKeyStorage {

	override fun implementCreateChildMergeStorage (childStorages: Sequence<Storage<String>>) : StringKeyMergeStorage {
		return StringKeyMergeStorage(childStorages)
	}

}