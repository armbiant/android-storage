package ic.storage.distributed


import ic.base.throwables.NotExistsException
import ic.storage.BaseStorage
import ic.storage.Storage
import ic.struct.list.List


abstract class DistributedStorage <Key> : BaseStorage<Key>() {


	@Throws(NotExistsException::class)
	abstract fun getStorageToReadOrThrowNotExists (key: Key) : Storage<Key>


	override fun implementGet (
		key: Key, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>
	) : Any? {

		val storageToRead = try {
			getStorageToReadOrThrowNotExists(key)
		} catch (t: NotExistsException) {
			return null
		}

		return storageToRead[key, constructorArgClasses, constructorArgs]

	}


}
