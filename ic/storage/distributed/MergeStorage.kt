package ic.storage.distributed


import ic.base.throwables.NotExistsException
import ic.interfaces.getter.getter1.untyped.ext.getOrSkip
import ic.interfaces.iterable.ext.copyConvert
import ic.interfaces.iterable.ext.findOrThrowNotExists
import ic.interfaces.iterable.ext.toList
import ic.storage.Storage
import ic.struct.list.List
import ic.struct.sequence.Sequence
import ic.struct.set.countable.CountableSet
import ic.struct.set.countable.union.UnionCountableSet


abstract class MergeStorage<Key> : DistributedStorage<Key>() {


	protected abstract val children : Sequence<Storage<Key>>


	override val keys : CountableSet<Key> get() {
		return UnionCountableSet( children.toList { it.keys } )
	}


	@Throws(NotExistsException::class)
	override fun getStorageToReadOrThrowNotExists (key: Key) : Storage<Key> {
		return children.findOrThrowNotExists { it.containsKey(key) }
	}


	protected open fun implementCreateChildMergeStorage (
		childStorages : Sequence<Storage<Key>>
	) = MergeStorage(childStorages)


	override fun implementGet (
		key: Key, constructorArgClasses: List<Class<*>>, constructorArgs: List<Any?>
	) : Any? {

		val value = super.implementGet(key, constructorArgClasses, constructorArgs)

		return if (value is Storage<*>) {

			implementCreateChildMergeStorage(
				children.copyConvert { it.getOrSkip(key) }
			)

		} else {

			value

		}

	}


}
