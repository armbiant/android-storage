@file:Suppress("FoldInitializerAndIfToElvis", "MoveVariableDeclarationIntoWhen")


package ic.storage.binding


import ic.base.throwables.AlreadyExistsException
import ic.interfaces.changeable.Changeable
import ic.storage.Storage
import ic.storage.writable.BaseWritableStorage
import ic.storage.writable.WritableStorage
import ic.struct.list.List


class BindingStorage<Key> (private val sourceStorage : WritableStorage<Key>)

	: BaseWritableStorage<Key>()

{


	override val keys get() = sourceStorage.keys


	override fun implementGet (
		key: Key,
		constructorArgClasses: List<Class<*>>,
		constructorArgs: List<Any?>
	) : Any? {

		val value : Any? = sourceStorage[key, constructorArgClasses, constructorArgs]

		return when (value) {

			null -> null

			is Storage<*> -> {

				@Suppress("UNCHECKED_CAST")
				val valueAsFolder = value as WritableStorage<Key>

				return BindingStorage(valueAsFolder)

			}

			is Changeable -> {
				value.onChangedEvent.watch { sourceStorage[key] = value }
				value
			}

			else -> value

		}

	}

	override fun getAsInt64 		(key: Key) = sourceStorage.getAsInt64(key)
	override fun getAsInt64OrNull 	(key: Key) = sourceStorage.getAsInt64OrNull(key)

	override fun set (key: Key, value: Any?) {

		sourceStorage[key] = when (value) {

			is Changeable -> {
				value.onChangedEvent.watch { sourceStorage[key] = value }
				value
			}

			else -> value

		}

	}


	@Throws(AlreadyExistsException::class)
	override fun createFolderOrThrowAlreadyExists (key: Key) : WritableStorage<Key> {

		return BindingStorage(sourceStorage.createFolderOrThrowAlreadyExists(key))

	}


}